FROM maven:3.6.3-jdk-11

RUN curl -O https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip \
    && unzip -q awscli-exe-linux-x86_64.zip \
    && ./aws/install \
    && rm -rf awscli-exe-linux-x86_64.zip \
    && curl -O https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mkdir -p $HOME/bin && mv ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin \
    && curl -O https://get.helm.sh/helm-v3.2.0-linux-amd64.tar.gz \
    && tar xvf helm-v3.2.0-linux-amd64.tar.gz \
    && mv ./linux-amd64/helm $HOME/bin/helm && export PATH=$PATH:$HOME/bin \
    && rm -rf linux-amd64 && rm -rf helm-v3.2.0-linux-amd64.tar.gz

CMD ["/bin/sh"]
